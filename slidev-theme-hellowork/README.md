# slidev-theme-hellowork

[[NPM version]](https://nexus.hellowork.com:8443/#browse/browse:npm-hosted:slidev-theme-hellowork)

An hellowork theme for [Slidev](https://github.com/slidevjs/slidev).

## Install

This theme is pre-configured in the gitlab template [template-slidev](../../../../../../../../DevOps/Templates/template-slidev).

Alternatively, if you wanna start a slidev project from scratch, you may add this theme to your project:

```bash
> npm init slidev

> cd your_project_name

> npm install slidev-theme-hellowork --registry https://nexus.hellowork.com:8443/repository/npm/  

```

Then, add the following frontmatter to your `slides.md`. 

<pre><code>---
theme: <b>hellowork</b>
---</code></pre>

Learn more about [how to use a theme](https://sli.dev/themes/use).

## Layouts

<pre><code>---
layout: <b>cover-1</b>
---</code></pre>

![cover-1](./screenshots/cover-1.png)

---

<pre><code>---
layout: <b>cover-2</b>
---</code></pre>

![cover-2](./screenshots/cover-2.png)

---

<pre><code>---
layout: <b>cover-3</b>
---</code></pre>

![cover-3](./screenshots/cover-3.png)

---

<pre><code>---
layout: <b>intro</b>
color: <b>beige</b>
---

# Votre titre

</code></pre>

![intro-beige](./screenshots/intro-beige.png)

---

<pre><code>---
layout: <b>intro</b>
color: <b>blue</b>
---

# Votre titre

</code></pre>

![intro-blue](./screenshots/intro-blue.png)

---

<pre><code>---
layout: <b>intro</b>
color: <b>green</b>
---

# Votre titre

</code></pre>

![intro-green](./screenshots/intro-green.png)

---

<pre><code>---
layout: <b>intro</b>
color: <b>purple</b>
---

# Votre titre

</code></pre>

![intro-purple](./screenshots/intro-purple.png)

---

<pre><code>---
layout: <b>section</b>
color: <b>beige</b>
---

# Titre partie
## Titre sous-partie

PHRASE D'ACCROCHE SI NECESSAIRE

</code></pre>

![section-beige](./screenshots/section-beige.png)

---

<pre><code>---
layout: <b>section</b>
color: <b>blue</b>
---

# Titre partie
## Titre sous-partie

PHRASE D'ACCROCHE SI NECESSAIRE

</code></pre>

![section-blue](./screenshots/section-blue.png)

---

<pre><code>---
layout: <b>image-right-1</b>
---</code></pre>

![image-right-1](./screenshots/image-right-1.png)

---

<pre><code>---
layout: <b>image-right-2</b>
---</code></pre>

![image-right-2](./screenshots/image-right-2.png)

---

<pre><code>---
layout: <b>image-right-3</b>
---</code></pre>

![image-right-3](./screenshots/image-right-3.png)

---

<pre><code>---
layout: <b>image-right-4</b>
---</code></pre>

![image-right-4](./screenshots/image-right-4.png)

---

<pre><code>---
layout: <b>image-right-5</b>
---</code></pre>

![image-right-5](./screenshots/image-right-5.png)

---

<pre><code>---
layout: <b>image-right-6</b>
---</code></pre>

![image-right-6](./screenshots/image-right-6.png)

---

![end](./screenshots/end.png)

---

you may give try to the sample slideshow [example.md](./example.md) :

```sh

> slidev --open example.md

```
