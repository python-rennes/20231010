---
theme: ./
layout: cover
---

---
layout: intro
# valid colors are 'green', 'blue', 'beige', 'purple'
color: green
---

# Votre titre

---
layout: section
# valid colors are 'green', 'blue', 'beige', 'purple'
color: beige
---

# Titre partie

## Titre sous-partie

PHRASE D'ACCROCHE SI NECESSAIRE

---
layout: image-right
---

# Demi-Slide avec image à droite

---
layout: image-right-2
---

# Demi-Slide avec image à droite

