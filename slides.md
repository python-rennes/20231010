---
theme: ./slidev-theme-hellowork
class: 'text-center'
highlighter: shiki
lineNumbers: false
info: |
  ## Meetup python rennes
drawings:
  persist: false
css: unocss
---

---
layout: intro
color: blue
---

## GitLab CI/CD sur un projet python microservice
### Retour sur 5 ans d'évolutions

<div class="abs-bl ml-36 mb-2 font-300">

Jean-Luc Tromparent

Antoine Coeur-Quetin

</div>
---

# GitLab (des débuts à nos jours)

<timeline
    :data="[
  { date: '2011', titles: ['Dmitriy Zaporozhets lance un projet de partage de code'] },
  { date: '2012', titles: ['Sytse Sijbrandij rejoint le projet','Dmitriy créé la première version de GitLab CI'] },
  { date: '2014', titles: ['Gitlab Inc.'] },
  { date: '2015', titles: ['GitLab 8.0'] },
  { date: '2018', titles: ['Microsoft rachete GitHub'] },
  { date: '2023', titles: ['GitLab 16.4'] },
]"
/>

https://handbook.gitlab.com/handbook/company/history/

---

# Un projet Python (de la préhistoire à nos jours)

### TARS : Totally Awesome Recommendation System

<timeline
    :data="[
  { date: '2014', titles: ['svn > git', 'redmine > gitlab', 'infogérance', 'v6 des sites emploi régionsjob', 'stack tracking: nodejs, mongodb'] },
  { date: '2015', titles: ['premier algo de recommandation', 'stack: c#, mongodb, redis'] },
  { date: '2017', titles: ['migration azure', 'lambda architecture', 'stack: eventhub, hdfs/spark'] },
  { date: '2018', titles: ['portage en python', 'microservice', 'stack: python, k8s'] },
  { date: '2022', titles: ['hellowork.com', 'poetry'] },
  { date: '2023', titles: ['monorepo', 'réduction de la dette'] },
]"
/>
<v-click>

![](/assets/timeline.png)

</v-click>

---

# Get started

- fichier .gitlab-ci.yml
  - configuration d'un pipeline de CI
  - séquence de stages et de jobs 

![pipeline](https://docs.gitlab.com/ee/ci/quick_start/img/pipeline_graph_v13_6.png)

https://docs.gitlab.com/ee/ci/quick_start/

---

# Un job de test

```yml

test-job:
  stage: test
  script:
    - poetry install
    - poetry run python -m pytest -v  --cov-report html:htmlcov --cov-fail-under $COVERAGE_MINIMUM_THRESHOLD_PERCENT
  coverage: '/^TOTAL.*\s+(\d+\%)\s*$/'
  artifacts:
    paths:
      - htmlcov/

```

![badge](/assets/coverage.svg)

---

# Architecture type de nos projets data

- un sous-projet librairie métier
  - périmètre datascientist
  - tests fonctionnels
- un sous-projet API (ou worker)
  - périmetre data-ingé
  - gestion des erreurs
  - scalabilité
  - containerization
  
---

# Les livrables de notre projet de recommandation

## → des librairies Python

```yml

publish-job:
  stage: release
  script:
    - poetry version $CI_COMMIT_TAG
    - poetry publish -r papaye --build
  only:
    - tags

```

---

# Les livrables de notre projet de recommandation

## → des images Docker


```yml

publish-job:
  stage: release
  script:
    - docker login ${NEXUS_URI} -u ${NEXUS_USER} -p ${NEXUS_PWD}
    - docker pull "${CI_PROJECT_NAME}:latest" || true
    - docker tag "${CI_PROJECT_NAME}:latest" "${NEXUS_URI}/${CI_PROJECT_NAME}:${CI_COMMIT_TAG}"
    - docker push "${NEXUS_URI}/${CI_PROJECT_NAME}:${CI_COMMIT_TAG}"
  only:
    - tags
```

--- 

# Multi-project pipelines

<div grid="~ cols-2 gap-4">
<div>

mylib/.gitlab-ci.yml 
```yml

trigger-job:
  stage: .post
  variables:
    LIB_VERSION: ${CI_COMMIT_TAG}
  trigger:
    project: DataScience/myProject/myApi
    branch: master
    strategy: depend
  only:
    - tags

```
</div>


<div v-click>

myapi/.gitlab-ci.yml
```yml

update_lib_version:
  stage: .pre
  script:
    - echo "Switch to lib version $LIB_VERSION"
    - poetry add mylib@$LIB_VERSION --lock
  artifacts:
    paths:
      - poetry.lock
      - pyproject.toml
  rules:
    - if: $LIB_VERSION


```

</div>
</div>

<div v-click>

![multi project pipeline](/assets/multi-project-pipeline.png)

</div>

---

# Versioning

```yml {|6}

release-job:
  stage: release
  script:
    # bumps the patch number of the semantic version of the project
    - poetry version patch
    - export NEW_VERSION=$(poetry version --short)
    # pushes the commit (asks the CI not to trigger a pipeline) and the tag after the version update
    - git add -A && git commit -m "🔖 Release $NEW_VERSION"
    - git push -o ci.skip -u origin HEAD:$CI_COMMIT_BRANCH
    - git tag -a $NEW_VERSION -m "Release $NEW_VERSION"
    - git push --tags origin $NEW_VERSION
    # pass version as artifact
    - echo $NEW_VERSION > .version
  artifacts:
    paths:
      - .version
  only:
    - master

```

---

# Architecture du projet REKO

![archi](/assets/internal-5.png)

--- 

# Déploiement

<div grid="~ cols-2 gap-4">
<div>

* environnement de production
  * infra k8s
  * template helm
  * déploiement depuis gitlab

</div>
<div v-click>

```yml
image:
  dispatch: 1.3.31
  packaging: 1.3.38
  jadexplorer: 1.0.6

  offer2offer: 1.3.40
  offer2user: 1.3.59
  user2offer: 1.3.23
  distributor: 1.3.15
  feedcool: 1.3.8
  feedcroo: 1.3.1
  feedlsq: 1.3.12
  reporter: 1.3.35
  shipper: 1.3.32
  watchdog: 1.3.5
  feedjade: 1.3.31
  upgradejade: 1.3.5
  
  newusers: 1.3.15
  refreshcache: 1.3.14
  batchupjade: 1.3.11
  keepintouch: 1.0.22
  regurgitor: 0.0.12
```
</div>
</div>


---
layout: two-cols
---

# Bilan 2018 - 2022

+ 🍏 Le projet scale bien
+ 🍏 Projet modulaire
- 🍎 MR portant sur plusieurs projets
- 🍎 Onboarding compliqué
- 🍎 Le déploiement n'est pas vraiment *continuous*

::right::

![volume](/assets/volume-offre.png)

---
layout: intro
color: blue
---

# Gitlab CI/CD
### Migration vers un monorepo
Pourquoi ?
---
layout: image-right
---
## Pourquoi un monorepo 
- 40 projets Gitlab
  - Duplication CI
  - Beaucoup de trigger 
- Pas de tag commun sur plusieurs projets


## Pourquoi maintenant
- Dette technique (Python 3.7)
- Projets transverses en étude : Log JSON, Lint, Sonarqube
- Migration Poetry
  - Impose : modifications de la CI
  - Permet : Imports relatifs dans la codebase

---
layout: image-right-5
---

## Le monorepo avec python

- Gerben Oostra (Gitlab) 
https://gitlab.com/gerbenoostra/poetry-monorepo


- David Vujic (Poetry)
https://github.com/DavidVujic/python-polylith

---
layout: image-right
---

## Procédure de migration 

1. Récupération de la codebase avec l'historique 
2. Migration Poetry
3. Variables de CI
4. Template Helm (kubernetes)
5. Déploiement en preprod
6. Mise en production


---
layout: image-right-2
---
## Structure du repository
```toml {all|11|2|3|4|5-9|10|9,14-25|7,20|2,25|all|12} 
TARS/
├─ common/
├─ k8s/
├─ worker/
│  ├─ jade/
│  │  ├─ app/
│  │  ├─ core/
│  │  ├─ README.md
│  │  └─ pyproject.toml
│  └─ watchdog/
├─ .gitlab-ci.yml
├─ pyproject.toml
└─ README.md

[tool.poetry]
name = "jade"
version = "1.5.0"
readme = "README.md"
packages = [
    { include = "jade", from = "core/" },
]
[tool.poetry.dependencies]
python = "~3.10"
requests = "2.25.1"
workerrq = {path="../../common/workerrq"}
```

---
layout: image-right-3
---

## pyproject.toml root
- Contient le paramétrage du formatter/LINT
- Permet de consolider les versions de dépendance
- Ne permet pas d'installer tous les projets 😢
```toml
[tool.poetry.group.worker.dependencies]
jade = {path="worker/jade"}
watchdog = {path="worker/watchdog"}

[tool.poetry.group.api.dependencies]
dispatch = {path="api/dispatch"}
packaging = {path="api/packaging"}

[tool.poetry.group.cronjob.dependencies]
refreshcache = {path="cronjob/refreshcache"}

[tool.poetry.group.dev.dependencies]
isort = "5.11.5"
yapf = "^0.40.1"
ruff = "^0.0.275"

[tool.isort]
line_length = 120
multi_line_output = 5
balanced_wrapping = false
```

---
layout: intro
color: blue
---

# Gitlab CI/CD
### Migration vers un monorepo
Le gitlab-ci.yml
---
layout: two-cols
---
```yaml{all|14-17|9-20|11-12,22-27|9-27|none}
stages:
  - test 🧪
  - check 🎨
  - build 👷
  - deploy preprod 🏗️
  - release 🔖
  - deploy production 🚀
  
.build:
  stage: build 👷
  extends:
    - .docker
  rules:
    - changes: 
        - ${PROJECT_TYPE}/${PROJECT_NAME}/**/*
        - k8s/${PROJECT_NAME}/**/*
        - common/**/*
  before_script:
    - *bump-version
    - export VERSION=$(poetry -C ${PROJECT_TYPE}/${PROJECT_NAME} version -s)

.docker:
  script:
    - docker login -u $GITLAB_USER -p $GITLAB_PASSWORD $ACR_URL
    - docker build . --build-arg PROJECT_PATH=${PROJECT_TYPE}/${PROJECT_NAME} --build-arg VERSION=$VERSION -t "$DOCKER_PATH/${PROJECT_NAME}:$VERSION"
    - docker push "$DOCKER_PATH/${PROJECT_NAME}:$VERSION"
```
::right::

```yaml{none|1-6,15-18|1-6,19-22|1-6,23-28|1-6,15-28|8-13}
.jade:
  variables:
    PROJECT_NAME: jade
    PROJECT_REGEX: /^jade\/.*/
    PROJECT_TYPE: worker
    NEED_VARS: "REDIS_RQ MONGODB_JADE_RW API_DISPATCH_URL JOBMACHINE_URL"

.watchdog:
  variables:
    PROJECT_NAME: watchdog
    PROJECT_REGEX: /^watchdog\/.*/
    PROJECT_TYPE: worker
    NEED_VARS: "REDIS_RQ SLACK_WEBHOOK"

jade 👷:
  extends:
    - .build
    - .jade
jade 🧪:
  extends:
    - .test
    - .jade
jade 🏗️:
  extends:
    - .preprod
    - .deploy
    - .jade
```
--- 
layout: image-right-4
---

![multi project pipeline](/assets/tars-multi-project-pipeline.png)


---
layout: two-cols
---
<v-clicks>

## Difficultés 😭

- Fusion des projets 
- Alignement des versions de lib
- Montée de version Python 3.8 globale
- Mise en place de la CI

### RAF
- Revoir la fusion de certains projets
- Manque les projets de la datascience
</v-clicks>

::right::

<v-clicks>

## Bien passé 😁

- Bonne généricité
- Fonctionnement ISO multi-repo
- Dé-duplication
  - De la CI
  - Des charts
  - Des dockerfiles
- Merci les tests
- Poetry (Imports relatif, Bump2version, Tests ...)
- Repo light (15 Mo)
- 15 projets déployables facilement

</v-clicks>


---
layout: intro
color: blue
---

# Merci
### Des questions ?

---
layout: end
---
